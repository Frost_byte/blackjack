import java.util.ArrayList;

public class Player {

	public double cash;
	// public Card[] hand = new Card[2];
	ArrayList<Card> hand = new ArrayList<Card>(0);
	ArrayList<Card> hand2 = null;
	public int handsum=0;
	public int handsum2=0;
	public double bet;
	public double bet2;
	public double insurancebet;
	public String name;
	public boolean blackjack=false;
	public boolean handbust;
	public boolean handbust2;
    public int cardbust=0;
    public int card21=0;
    public int hand2cardbust=0;
    public int hand2card21=0;
   
	

	// Above are all the necessary fields that this class file will use
	
	public void blackjacktest()
	{ //used for testing purposes
		this.handsum=21;
	}

	public Player(String name, double cash) {
		this.name = name;
		this.cash = cash;
		// This is the Player constructor
		// Every player created will be assigned a name and given $100 as the
		// starting balance for cash
	}

	public void bet(double a) {

		this.bet = a;
		// This is the bet constructor
		// It will initialize a value to the this.bet variable once a player
		// inputs his/her bet
	}

	public int cardValue(Card n) {
		return n.getValue();
		// this method returns the face value of the card
		// essential when determining if there is going to be insurance, splitting, etc.
	}

	public double getBalance() {
		return this.cash;
		// Simple getter method, returns the current balance of the player's first hand 
	}

	public double win() {
		this.cash = this.cash + this.bet;
		return this.cash;
		// if the player wins or ties, this method is called, and the player's
		// balance is increased
		// this method is only called if the player did not split
	}

	public double lose() {
		this.cash = this.cash - this.bet;
		return this.cash;
		// if the player loses against the dealer, this method is called, and
		// the player's balance is decreased
		// this method is only called if the player did not split
	}

	public void addCard(Card n) // adds a card to the player's first hand
	{
		System.out.println(n.toString());
		hand.add(n);
		// this.handsum = this.handsum + n.getValue();
		if (n.getValue() == 11 && this.handsum > 10) {
			this.handsum = this.handsum + 1;
		}
		if (n.getValue() == 11 && this.handsum <= 10) {
			this.handsum = this.handsum + n.getValue();
		}
		if (n.getValue() != 11) {
			this.handsum = this.handsum + n.getValue();
		}
		// this method adds a card to the player's hand

	}

	public String getName() {
		return this.name;
		// Another simple getter method that returns the name of the player in
		// question
	}

	public int gethandsum() {
		return handsum;
		// Returns the sum of the face values of the cards in the player's first hand
	}

	public int setScore() {
		return handsum = 0;
		// This method needs to be called at the beginning of every new round
		// Players need to throw out their old cards before being dealt 2 new
		// ones
	}
	

	public void doubleDownBet(double a) {// method that is called whenever
											// player wants to double down

		double b=(a/100.0)*this.bet;
		this.bet=this.bet+b;

	}

	public double getBet() {// simple getter method that returns current value
							// of bet of the first hand
		return this.bet;
	}

	public void insuranceBet(double a) {// this method is called when player
										// wants insurance

		this.insurancebet=a;
	}
	
	public double winInsurance()
	{
		this.insurancebet=this.insurancebet*2.0;
		this.cash=this.cash+this.insurancebet;
		return this.cash;
		//call this if the player wins insurance
	}
	
	public double loseInsurance()
	{
		this.cash=this.cash-this.insurancebet;
		return this.cash;
		//call this if the player loses the insurance
	}
	
	public void isBlackJack()
	{
		this.blackjack=true; // this method initializes boolean blackjack variable to true		
		                     // if player does have a blackjack
	}
	
	public double winBlackJack()
	{
		this.bet=this.bet*1.5;
		this.cash=this.cash+this.bet;
		return this.cash;
		/* If player does win blackjack, bet variable must be multiplied by
		 * 1 and a half times
		 * Afterwards, add this bet to cash variable to get ending balance for the round
		 */
	}
	
	public void cardsBust()
	{
		this.cardbust=this.cardbust+1;
		//increments cardbust by 1
	}
	
	public void cards21()
	{
		this.card21=this.card21+1;
		//increments card21 by 1
	}
	
	public int getcardsBust()
	{
		return this.cardbust;
		//returns a number of cards that, if added to the player's hand, will cause the hand to bust
	}
	
	public int getcards21()
	{
		return this.card21;
		//returns a number of cards that, if added to the player's hand, will increase the hand's value to 21
	}
	
	public int setcardsBust()
	{
		return this.cardbust=0;
		//resets this variable or else percentages will be wrong
	}
	
	public int setcards21()
	{
		return this.card21=0;
		//resets this variable or else percentages will be wrong
	}
	

	/* All of the following code
	 * is related to splitting
	 */
	
	public int setcardsBusthand2()
	{
		return this.hand2cardbust=0;
		//same as above method
	}
	
	public int setcards21hand2()
	{
		return this.hand2card21=0;
		//resets this variable or else percentages will be wrong
	}
	
	public void hand2cardsBust()
	{
		this.hand2cardbust=this.hand2cardbust+1;
		//look at the method for hand 1
	}
	
	public void hand2cards21()
	{
		this.hand2card21=this.hand2card21+1;
		//look at the method for hand 1
	}
	public int hand2getcardsBust()
	{
		return this.hand2cardbust;
		//look at the appropriate method for hand 1
	}
	
	public int hand2getcards21()
	{
		return this.hand2card21;
		//look at the appropriate method for hand 1
	}
	public void newHand()
	{
		this.hand2 = new ArrayList<Card>(0);
		// This method creates a new hand for the player to use
	}
	
	public void setScore1()
	{
		this.handsum2=0;
		//resets hand 2's value back to 0
	}
	
	public void bet()
	{
		this.bet2=this.bet;
		// If a player decides to split the hand
		// another bet must be created for that second hand
		// the second bet must be equal to the first bet for the first hand
	}
	
	public void removeCardSplit(Card n)
	{
		this.hand.remove(n);
		if (n.getValue() == 11 && this.handsum > 10) {
			this.handsum = this.handsum - 1;
		}
		if (n.getValue() == 11 && this.handsum <= 10) {
			this.handsum = this.handsum - n.getValue();
		}
		if (n.getValue() != 11) {
			this.handsum = this.handsum - n.getValue();
		}
		
		// 1 card must be taken from the first hand
		// You can't have duplicates!
	}

	public void addCardSplit(Card n) {
		this.hand2.add(n);
		if (n.getValue() == 11 && this.handsum2 > 10) {
			this.handsum2 = this.handsum2 + 1;
		}
		if (n.getValue() == 11 && this.handsum2 <= 10) {
			this.handsum2 = this.handsum2 + n.getValue();
		}
		if (n.getValue() != 11) {
			this.handsum2 = this.handsum2 + n.getValue();
		}
		// adds the card taken from player's first hand to the player's second hand
		// not to be confused with hitting the player's second hand
	}
	
	public void addCard2(Card n) 
	{
		this.hand2.add(n);
		if (n.getValue() == 11 && this.handsum2 > 10) {
			this.handsum2 = this.handsum + 1;
		}
		if (n.getValue() == 11 && this.handsum2 <= 10) {
			this.handsum2 = this.handsum2 + n.getValue();
		}
		if (n.getValue() != 11) {
			this.handsum2 = this.handsum2 + n.getValue();
		}
		// this method adds a card to the player's second hand
	}
	
	public int gethandsum2()
	{
		return this.handsum2;
		// Another getter method that returns value of cards in second hand
	}
	
	public double splitWin()
	{
		this.bet=this.bet*2.0;
		this.cash=this.cash+this.bet;
		return this.cash;
		// a possible scenario
	}
	
	public double splitLose()
	{
		this.bet=this.bet*2.0;
		this.cash=this.cash-this.bet;
		return this.cash;
		// a possible scenario
	}
	
	public double splitTie()
	{
		this.cash=this.cash-this.bet;
		return this.cash;
		// a possible scenario
	}
	
	public double splitTie2()
	{
		this.cash=this.cash+this.bet;
		return this.cash;
		// a possible scenario
	}
	
	/*public void isHandBust()
	{
		this.handbust=true;
	}
	
	public void isHandBust2()
	{
		this.handbust2=true;
	}*/

}
