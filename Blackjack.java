
public class Blackjack {
	// keep player bank rolls AND deck OUTSIDE of the do while loop or else
	// you'll be resetting everything
	public static void main (String[] args)
	{
		
		Deck deck=new Deck();
	//	deck.shuffle();
		//System.out.println(deck);
		Dealer dealer=new Dealer();
	
		System.out.println("This is a simplified blackjack casino game that's coded in Java.");
		System.out.println("How many players do you want to have? Max is 3 players.");
		int players=IO.readInt();
		while (players>3||players<0)
		{
			System.out.println("Too many or too few players. Enter a new number.");
			players=IO.readInt();
		}		
		
		Player[] players1 = new Player[players];
		
		for (int i=0;i<players1.length;i++) //creates and initializes (enter a name, receive bankroll) a player
		{
			System.out.println("What's the name of Player "+(i+1));
			String name=IO.readString();
            int cash=100;
			Player c=new Player(name, cash);
			players1[i] = c;
			
		}
		
		System.out.println();
		System.out.println("Before the game starts, here are the general guidelines of Blackjack.");
		
		do
		{
		System.out.println("When a new round begins, the dealer takes the deck and shuffles it into a random order.");
		System.out.println();
		System.out.println("Each player will have to input his/her name.");	
		System.out.println();
		System.out.println("Each player will be given a bankroll of $100.");
		System.out.println();
		System.out.println("All of the players will then place their bets.");
		System.out.println();
		System.out.println("Afterwards, the dealer will deal 2 cards to each player face up.");
		System.out.println();
		System.out.println("Then the dealer will give himself 2 cards... one face up and the other is face down");
		System.out.println();
		System.out.println("If the dealer's face up card is an Ace, all of the players have the option of taking insurance.");
		System.out.println();
		System.out.println("Essentially, for every dollar the player bets for insurance, the player will receive/lose double the amount");
		System.out.println();
		System.out.println("if he or she wins/loses. Example, if a player bets $5 for insurance, the player will win/lose an additional $10 at the end of the round.");
		System.out.println();
		System.out.println("Taking insurance is not necessary in Blackjack; it's entirely up to the player.");
		System.out.println();
		System.out.println("Players can bet up to 50% of their original bet if they choose to take insurance. ");
		System.out.println();
		System.out.println("If the player is dealt two cards of equal value, the player has the option of splitting.");
		System.out.println();
		System.out.println("Player places one card in each hand and plays both hands independently.");
		System.out.println();
		System.out.println("If the player chooses to split, the player can no longer choose to double down.");
		System.out.println();
		System.out.println("After all of these steps are taken, the dealer will ask every player if");
		System.out.println();
		System.out.println("he or she wishes to hit, stand, or double down.");
		System.out.println();
		System.out.println("If a player chooses to hit, the dealer will give the player 1 card from the deck.");
		System.out.println();
		System.out.println("The player can choose to hit as many times as he/she wants to (as long as there are still cards in the deck).");
		System.out.println();
		System.out.println("If a player chooses to stand, the player voluteers to skip his/her turn");
		System.out.println();
		System.out.println("and the dealer will move on to the next player.");
		System.out.println();
		System.out.println("If a player chooses to double down, the player will be asked to make a new bet.");
		System.out.println();
		System.out.println("The player can bet up to 100% of their original bet amount.");
		System.out.println();
		System.out.println("After a player places the bet, the dealer will give the player one final card");
		System.out.println();
		System.out.println("and the player in question will be forced to stand.");
		System.out.println();
		System.out.println("The player cannot decide to hit/stand and then double down. The player can only choose to double down once.");
		System.out.println();
		System.out.println("During the hitting/standing/doubling down process, the program will keep track of the player's score.");
		System.out.println();
		System.out.println("The score is the sum of the face card values in the player's hand.");
		System.out.println();
		System.out.println("If the player's score goes above 21 during this process, the player in question will immediately bust and lose the round.");
		System.out.println();
		System.out.println("The dealer will then skip to the next player.");
		System.out.println();
		System.out.println("Once everyone has taken their turn, it's time for the dealer to play.");
		System.out.println();
		System.out.println("The dealer flips his face down card over.");
		System.out.println();
		System.out.println("The dealer will repeatedly hit if his score is less than 17 and stand if his score is greater than 17");
		System.out.println();
		System.out.println("If the dealer busts, anyone who didn't bust earlier win the round.");
		System.out.println();
		System.out.println("If the dealer does not bust, all of the player's scores are compared to that of the dealer's");
		System.out.println();
		System.out.println("If the player's score is less than that of the dealer's score, he loses.");
		System.out.println();
		System.out.println("His balance will be decreased by however much he bet.");
		System.out.println();
		System.out.println("If the player ties, he won't win but his cash balance will increase by however much he bet.");
		System.out.println();
		System.out.println("If the player's score is greater, the player wins and his cash balance increases by however much he bet.");
		System.out.println();
		System.out.println("Obviously, if the player busted, the player will instantly lose.");
		System.out.println();
		System.out.println("Afterwards, everyone can decide if they want to play again.");
		System.out.println();
		System.out.println("If a player's balance is less than or equal to zero");
		System.out.println();
		System.out.println("the player in question will no longer be allowed to play.");
		System.out.println();
		System.out.println("No buying back, etc.");
		System.out.println();
		System.out.println("Those are all the guidelines/hints.");
		System.out.println();
		System.out.println("If anyone gets stuck during the hitting/standing/doubling down procedure");
		System.out.println();
		System.out.println("They can request a hint. But, they can only do this at the beginning of their turn!");
		System.out.println();
		System.out.println("If a player chose to split, he/she can get 2 hints (1 hint for each hand)!");
		System.out.println();
		System.out.println("This game will be using the Advanced Omega II Card Counting System.");
		System.out.println();
		System.out.println("Players can google individual value of cards in this system to keep track of the card count.");
		System.out.println();
		System.out.println("Hit y for yes if anyone wants this message to be repeated. If not, hit any other letter to begin the game!");
		}
		while (IO.readChar()=='y');
		
		do	
		{
			deck.shuffle(); //calls the shuffle method in the deck class
			
			System.out.println();
			System.out.println();
			System.out.println("So far, the card count for this round is " + deck.getCardCount());
			System.out.println("Does everyone want a hint on what to do? Hit y if you guys want to or any other letter if not");
			char cardCount=IO.readChar();
			if(cardCount=='y')	
			{
			    if (deck.getCardCount()>=2)
			    {
			    	System.out.println("The card count so far is positive.");
			    	System.out.println("Everyone should place higher bets.");
			    	System.out.println();
			    }
			    
			    if (deck.getCardCount()<2)
			    {
			    	System.out.println("The card count so far is not very large.");
			    	System.out.println("To stay on the safe side, place smaller bets.");
			    	System.out.println();
			    }
			}
			for (int i=0;i<players1.length;i++)
			{
				System.out.println(players1[i].getName() + "'s balance as of this current round is " + players1[i].getBalance());
			}
			System.out.println("How much money does each player want to bet?");
			System.out.println("Enter in order, as first input is for player 1, etc.");
			for (int i=0;i<players1.length;i++)
			{
				if (players1[i].getBalance()>0)
				{
				System.out.println(players1[i].getName() + ", how much do you want to bet?");
				double u=IO.readDouble();
				while (u>players1[i].getBalance()||u<=0)
				{
					System.out.println("You bet the wrong amount of money.");
					System.out.println("You have to bet a certain amount of money, but your bet CANNOT be greater than your current balance.");
					u=IO.readDouble();
				}
                players1[i].bet(u);
				}
				
				if (players1[i].getBalance()<=0)
				{
				System.out.println(players1[i].getName() + ", you're not allowed to bet.");
				}
			}	
		System.out.println("Dealer is to deal two cards to each player.");
		System.out.println();
		
		for (int i=0;i<players1.length;i++)
		{	
			players1[i].setScore();
			players1[i].setcardsBust();
            players1[i].setcards21();
            //these methods need to be called because everything must be reset at the beginning of each round
		}
		
		for (int i=0;i<players1.length;i++)
		{
			players1[i].setScore1();
			players1[i].setcards21hand2();
            players1[i].setcardsBusthand2();
            //these methods need to be called for the same reason above
		}
		
		//players1[i].addCard(deck.deal());
		//players1[i].addCard(deck.deal());
		
		//Dealer will now deal 2 cards to each player and then 2 (1 face up 1 face down) to himself
		for (int i=0;i<players1.length;i++)
		{
			
			if (deck.isEmpty()==true)
			{
				System.out.println("We ran out of cards!");
				return;
			}
			if (players1[i].getBalance()>0)
			{
			Card b=deck.deal();	
			players1[i].addCard(b);
			Card c=deck.deal();
			players1[i].addCard(c);
			int r=players1[i].cardValue(b);
			int y=players1[i].cardValue(c);
			//System.out.println("value of card 1 is " + r + " and 2 is " + y);
			System.out.println(players1[i].getName() + " the sum of your cards so far is " + players1[i].gethandsum());
			if (r+y==21)
			{
				System.out.println("The sum of the cards you have is 21!");
				System.out.println(players1[i].getName() + ", you have blackjack!");
				players1[i].isBlackJack();
				//players1[i].blackjacktest();
			}
			
			
			if (r==y)
			{
				System.out.println(players1[i].getName() + ", you have the option of splitting your hand.");
				System.out.println("Do you want to split your hand?");
				System.out.println("Hit y for yes, n for no.");
				char choice4=IO.readChar();
                while (choice4!='y'&&choice4!='n')
                {
                	System.out.println("Wrong letter, enter again.");
                	choice4=IO.readChar();
                }
				if (choice4=='y')
				{
					players1[i].newHand();
					players1[i].removeCardSplit(b);
					players1[i].addCardSplit(b);
					Card d=deck.deal();
                    players1[i].addCard(d);
                    System.out.println(players1[i].getName() + ", the sum of the values of your first hand is " + players1[i].gethandsum());
				}
                if (choice4=='n')
                {
                	System.out.println();
                	{continue;}
                }

			}
			System.out.println();
			}
			if (players1[i].getBalance()<=0)
			{
				System.out.println(players1[i].getName() + ", you're not allowed to play anymore.");
			}
		}
		
		System.out.println();
		System.out.println("The dealer now has to deal two cards to himself.");
		dealer.setScore();
		System.out.println();
		
		Card a=deck.deal();
		dealer.addCard(a);
		
		Card b=deck.deal();
		System.out.println();
		System.out.println("The value of the face up in the dealer's hand is " + dealer.gethandsum());
		
		if (dealer.gethandsum()==11)
		{
			System.out.println("The dealer's card that is face up is an Ace!");
			System.out.println("Each player now has the option of taking insurance.");
			for (int i=0;i<players1.length;i++)
			{
		System.out.println(players1[i].getName() + ", how much money do you want to include for insurance?");		
		System.out.println("If you don't want insurance, enter 0 for your number.");
		double g=IO.readDouble();
        while (g>(players1[i].getBet()/2.0))
        {
        System.out.println("You bet too high! Enter another number.");
        g=IO.readDouble();
        }
        players1[i].insuranceBet(g);
        System.out.println();
		}
		}
		
		System.out.println();
		//for (int i=0;i<deck.reducedDeck().length;i++)
		//{
			//System.out.println(i + ". " + deck.reducedDeck()[i]);
			//Prints out the reduced deck; I just used it for debugging
		//}
		//int pp=deck.reducedDeck()[0].getValue();
		//System.out.println(pp);
		System.out.println();
		System.out.println("Dealing cards.");
		
		
		
		for (int i=0;i<players1.length;i++)
		{
			if(players1[i].getBalance()>0&&players1[i].hand2!=null)//player chose to split
			{
				System.out.println(players1[i].getName() + ", you'll be playing with your original hand first.");
				System.out.println("Your first hand has a sum of " + players1[i].gethandsum());
				System.out.println("Do you want a hint?");
				System.out.println("Hit i if you want a hint or any other letter if not.");
				char hint=IO.readChar();
				
				if (hint=='i')
				{
					System.out.println(players1[i].getName() + ", you requested a hint.");
					
					if (players1[i].gethandsum()<=11)
					{
						System.out.println("You're pretty much safe to hit.");
						System.out.println("There's no point to stand.");
						
						int q=0;
						while (deck.reducedDeck()[q]!=null&&q<deck.reducedDeck().length)
						{
							
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()==21)
							{
								//this if statement will run through the loop and keep track of all the cards
								//that, if added to the player's hand, will increase the hand's value to 21
								players1[i].cards21();
							}
							q++;
						}
					    
						int decklength=0;
						while (deck.reducedDeck()[decklength]!=null&&q<deck.reducedDeck().length)
						{
							//this loop will determine the length of the reduced deck(to calculate probabilities)
							//if a card value in the reduced Deck has a value of null, the loop will stop
							//if it doesn't, I'll get a null pointer exception
							decklength++;
						}
						
						double x=0;
						double y=players1[i].getcards21()/(double)decklength; //division
						double w=1-y;
						System.out.println("The probability of you busting is " + x*100 + "%"); //A player cannot bust if his hand has a score of 11 or less
						System.out.println("The probability of your hand getting a sum of 21 after hitting is " + y*100 + "%");
						System.out.println("Probability of your hand not getting a sum of 21 after hitting is " + w*100 + "%");
						
						System.out.println();
						
					}
					
					if (players1[i].gethandsum()<=18&&players1[i].gethandsum()>=12)
					{
						System.out.println("You might not want to hit here, you might bust!");
						System.out.println("You might want to stand.");
						//Card[] jello=deck.reducedDeck();
						
						int q=0;
						while (deck.reducedDeck()[q]!=null&&q<deck.reducedDeck().length)
						{
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()>21&&deck.reducedDeck()[q].getValue()!=11)
							{
								//this loop keeps track of all the cards in the reduced deck that
								//if added to the player's hand, will cause the player
								//to bust and lose the round
								players1[i].cardsBust();
							}
							
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()==21)
							{
								players1[i].cards21();
							}
							q++;
						}
					    
						int decklength=0;
						while (deck.reducedDeck()[decklength]!=null&&q<deck.reducedDeck().length)
						{
							decklength++;
						}
						
						double x=players1[i].getcardsBust()/(double)decklength;//division
						double y=players1[i].getcards21()/(double)decklength;
						double z=1-x;
						double w=1-y;
						System.out.println("Probability of you not busting is " + z*100 + "%");
						System.out.println("Probability of your hand not getting a sum of 21 is " + w*100 + "%");
						System.out.println("The probability of you busting is " + x*100 + "%");
						System.out.println("The probability of your hand getting a sum of 21 after hitting is " + y*100 + "%");
						
						System.out.println();
					}
					
					if (players1[i].gethandsum()>=19&&players1[i].gethandsum()<=21)
					{
						System.out.println("Don't hit. There's a high chance you may bust.");
						System.out.println("You should most definitely stand!");
						//Card[] jello=deck.reducedDeck();
						int q=0;
						while (deck.reducedDeck()[q]!=null&&q<deck.reducedDeck().length)
						{
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()>21&&deck.reducedDeck()[q].getValue()!=11)
							{
								players1[i].cardsBust();
							}
							
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()==21)
							{
								players1[i].cards21();
							}
							q++;
						}
					    
						int decklength=0;
						while (deck.reducedDeck()[decklength]!=null&&q<deck.reducedDeck().length)
						{
							decklength++;
						}
						
						double x=players1[i].getcardsBust()/(double)decklength;
						double y=players1[i].getcards21()/(double)decklength;
						double z=1-x;
						double w=1-y;
						System.out.println("Probability of you not busting is " + z*100 + "%");
						System.out.println("Probability of your hand not getting a sum of 21 is " + w*100 + "%");
						
						System.out.println("The probability of you busting is " + x*100 + "%");
						System.out.println("The probability of your hand getting a sum of 21 after hitting is " + y*100 + "%");
						
						System.out.println();
					}
					
					
				}
				
				System.out.println("Hit h if you want to hit or hit s if you want to stand.");
				char choice=IO.readChar();
				if (choice=='h')
				{
					if (deck.isEmpty()==true)
					{
						System.out.println("We ran out of cards!");
						return;
					}
				Card o=deck.deal();
                players1[i].addCard(o);
                System.out.println(players1[i].gethandsum());
                System.out.println("Do you want to keep on hitting? Hit y for yes, n for no.");
                char choice2=IO.readChar();
                while (IO.readChar()=='y')
                {
                	Card t=deck.deal();
                    players1[i].addCard(t);
                    System.out.println(players1[i].gethandsum());
                   if (players1[i].gethandsum()>21)
                   {
                	   System.out.println("Your first hand busted!");
                	  // players1[i].isHandBust();
                	   {break;}
                	   
                   }
                   
                   if (deck.isEmpty()==true)
                   {
                	   System.out.println("We ran out of cards!");
                	   return;
                   }
                }
                
                if (choice2=='n')
                {
                	//System.out.println("Moving on to the second hand.");
                	System.out.println();
                }
                if (choice=='s')
                {
                	System.out.println("Moving on to the second hand.");
                	System.out.println();
                }
				}
				
				System.out.println("You're now playing with your second hand.");
				System.out.println("The sum of your second hand is " + players1[i].gethandsum2());
				System.out.println("Hit i if you want a hint or any other letter if not.");
				char hint1=IO.readChar();
				
				if (hint1=='i')
				{
					System.out.println(players1[i].getName() + ", you requested a hint.");
					
					if (players1[i].gethandsum2()<=11)
					{
						System.out.println("You're pretty much safe to hit.");
						System.out.println("There's no point to stand.");
						int q=0;
						while (deck.reducedDeck()[q]!=null&&q<deck.reducedDeck().length)
						{
							
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum2()==21)
							{
								//this time, I'm keeping track of the number of cards
								//that, if added to the player's SECOND hand
								//will increase the hand's value to 21
								
								players1[i].hand2cards21();
							}
							q++;
						}
					    
						int decklength=0;
						while (deck.reducedDeck()[decklength]!=null&&q<deck.reducedDeck().length)
						{
							decklength++;
						}
						
						double x=0;
						double y=players1[i].hand2getcards21()/(double)decklength;
						double z=1-x;
						double w=1-y;
						System.out.println("Probability of you not busting is " + z*100 + "%");
						System.out.println("Probability of your hand not getting a sum of 21 is " + w*100 + "%");
						
						System.out.println("The probability of you busting is " + x*100 + "%");
						System.out.println("The probability of your hand getting a sum of 21 after hitting is " + y*100 + "%");
						
						
						
						
						System.out.println();
					}
					
					if (players1[i].gethandsum2()>=12&&players1[i].gethandsum2()<=18)
					{
						System.out.println("You might not want to hit here, you might bust!");
						System.out.println("You might want to stand.");
						int q=0;
						while (deck.reducedDeck()[q]!=null&&q<deck.reducedDeck().length)
						{
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum2()>21&&deck.reducedDeck()[q].getValue()!=11)
							{
								players1[i].hand2cardsBust();
							}
							
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum2()==21)
							{
								players1[i].hand2cards21();
							}
							q++;
						}
					    
						int decklength=0;
						while (deck.reducedDeck()[decklength]!=null&&q<deck.reducedDeck().length)
						{
							decklength++;
						}
						
						double x=players1[i].hand2getcardsBust()/(double)decklength;
						double y=players1[i].hand2getcards21()/(double)decklength;
						double z=1-x;
						double w=1-y;
						System.out.println("Probability of you not busting is " + z*100 + "%");
						System.out.println("Probability of your hand not getting a sum of 21 is " + w*100 + "%");
						
						System.out.println("The probability of you busting is " + x*100 + "%");
						System.out.println("The probability of your hand getting a sum of 21 after hitting is " + y*100 + "%");
						
						
						
						System.out.println();
					}
					
					if (players1[i].gethandsum2()>=19&&players1[i].gethandsum2()<=21)
					{
						System.out.println("Don't hit. There's a high chance you may bust.");
						System.out.println("You should most definitely stand!");
						int q=0;
						while (deck.reducedDeck()[q]!=null&&q<deck.reducedDeck().length)
						{
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum2()>21&&deck.reducedDeck()[q].getValue()!=11)
							{
								players1[i].hand2cardsBust();
							}
							
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum2()==21)
							{
								players1[i].hand2cards21();
							}
							q++;
						}
					    
						int decklength=0;
						while (deck.reducedDeck()[decklength]!=null&&q<deck.reducedDeck().length)
						{
							decklength++;
						}
						
						double x=players1[i].hand2getcardsBust()/(double)decklength;
						double y=players1[i].hand2getcards21()/(double)decklength;
						double z=1-x;
						double w=1-y;
						System.out.println("Probability of you not busting is " + z*100 + "%");
						System.out.println("Probability of your hand not getting a sum of 21 is " + w*100 + "%");
						
						System.out.println("The probability of you busting is " + x*100 + "%");
						System.out.println("The probability of your hand getting a sum of 21 after hitting is " + y*100 + "%");
						
						
							
						System.out.println();
					}
					
					
				}
				
				
				System.out.println("Hit or stand?");
				char choice3=IO.readChar();

                if (choice3=='s')
                {
                System.out.println();
                {continue;}
                }
                
                if (choice3=='h')
                {
                	if (deck.isEmpty()==true)
                	{
                		System.out.println("We ran out of cards!");
                		return;
                	}
                	Card o=deck.deal();
                    players1[i].addCard2(o);
                    System.out.println(players1[i].gethandsum2());
                    System.out.println("Do you want to keep on hitting? Y for yes, n for no.");
                    char choice4=IO.readChar();
                    
                    while (IO.readChar()=='y')
                    {
                    	Card t=deck.deal();
                        players1[i].addCard2(t);
                        System.out.println(players1[i].gethandsum2());
                       if (players1[i].gethandsum2()>21)
                       {
                    	   System.out.println("Your second hand busted!");
                    	  // players1[i].isHandBust2();
                    	   {break;}
                    	   
                       }
                       
                       if (deck.isEmpty()==true)
                       {
                    	   System.out.println("We ran out of cards!");
                    	   return;
                       }
                    }
                    
                    if (choice4=='n')
                    {
                    	System.out.println();
                    	{continue;}
                    }
                    
                    
                }

			}
			if (players1[i].getBalance()>0&&players1[i].hand2==null)//did not split
			{
			System.out.println(players1[i].getName() + ", do you want to hit, stand, or use the double down option?");
			System.out.println(players1[i].getName() +" the value of the cards in your hand is " + players1[i].gethandsum());
			System.out.println("Hit i if you want a hint or any other letter if not.");
			char hint=IO.readChar();
            if (hint=='i')
				{
					System.out.println(players1[i].getName() + ", you requested a hint.");
					
					if (players1[i].gethandsum()<=11)
					{
						System.out.println("You're pretty much safe to hit.");
						System.out.println("There's no point in standing.");
						int q=0;
						while (deck.reducedDeck()[q]!=null&&q<deck.reducedDeck().length)
						{
							
							
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()==21)
							{
								players1[i].cards21();
							}
							q++;
						}
					    
						int decklength=0;
						while (deck.reducedDeck()[decklength]!=null&&q<deck.reducedDeck().length)
						{
							decklength++;
						}
						
						double x=0;
						double y=players1[i].getcards21()/(double)decklength;
						double z=1-x;
						double w=1-y;
						System.out.println("Probability of you not busting is " + z*100 + "%");
						System.out.println("Probability of your hand not getting a sum of 21 is " + w*100 + "%");
						
						System.out.println("The probability of you busting is " + x*100 + "%");
						System.out.println("The probability of your hand getting a sum of 21 after hitting is " + y*100 + "%");
						
						System.out.println();
					}
					
					if (players1[i].gethandsum()>=12&&players1[i].gethandsum()<=18)
					{
						System.out.println("You might not want to hit here, you might bust!");
						System.out.println("You might want to stand.");
						int q=0;
						while (deck.reducedDeck()[q]!=null&&q<deck.reducedDeck().length)
						{
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()>21&&deck.reducedDeck()[q].getValue()!=11)
							{
								players1[i].cardsBust();
							}
							
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()==21)
							{
								players1[i].cards21();
							}
							q++;
						}
					    
						int decklength=0;
						while (deck.reducedDeck()[decklength]!=null&&q<deck.reducedDeck().length)
						{
							decklength++;
						}
						
						double x=players1[i].getcardsBust()/(double)decklength;
						double y=players1[i].getcards21()/(double)decklength;
						double z=1-x;
						double w=1-y;
						System.out.println("Probability of you not busting is " + z*100 + "%");
						System.out.println("Probability of your hand not getting a sum of 21 is " + w*100 + "%");
						
						System.out.println("The probability of you busting is " + x*100 + "%");
						System.out.println("The probability of your hand getting a sum of 21 after hitting is " + y*100 + "%");
						
						System.out.println();
					}
					
					if (players1[i].gethandsum()>=19&&players1[i].gethandsum()<=21)
					{
						System.out.println("Don't hit. There's a high chance you may bust.");
						System.out.println("You should most definitely stand!");
						int q=0;
						while (deck.reducedDeck()[q]!=null&&q<deck.reducedDeck().length)
						{
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()>21&&deck.reducedDeck()[q].getValue()!=11)
							{
								players1[i].cardsBust();
							}
							
							if (deck.reducedDeck()[q].getValue()+players1[i].gethandsum()==21)
							{
								players1[i].cards21();
							}
							q++;
						}
					    
						int decklength=0;
						while (deck.reducedDeck()[decklength]!=null&&q<deck.reducedDeck().length)
						{
							decklength++;
						}
						
						double x=players1[i].getcardsBust()/(double)decklength;
						double y=players1[i].getcards21()/(double)decklength;
						double z=1-x;
						double w=1-y;
						System.out.println("Probability of you not busting is " + z*100 + "%");
						System.out.println("Probability of your hand not getting a sum of 21 is " + w*100 + "%");
						
						System.out.println("The probability of you busting is " + x*100 + "%");
						System.out.println("The probability of your hand getting a sum of 21 after hitting is " + y*100 + "%");
						
						System.out.println();
					}
					
					
				}
			System.out.println("Hit h for hit, s for stand, or d for double down.");
			char choice=IO.readChar();
			
			while (choice!='h'&&choice!='s'&&choice!='d')
	            {
	            System.out.println("Hit h or s or d");
	            choice=IO.readChar();
	            }
			if (choice=='d')
			{
				System.out.println("How much do you want to increase your bet by?");
				System.out.println("For example, enter 5 if you want to increase bet by 5%.");
				double x=IO.readDouble();
				while (x>100)
				{
					System.out.println("Percentage is too large. Enter again.");
					x=IO.readDouble();
					
				}
				players1[i].doubleDownBet(x);
				
				if (deck.isEmpty()==true)
				{
					System.out.println("We ran out of cards!");
					return;
				}
				//players1[i].addCard(deck.deal());
				Card z=deck.deal();
				players1[i].addCard(z);
				
				System.out.println("Your new bet amount is now " + players1[i].getBet());
				System.out.println(players1[i].getName() + ", the value of the cards in your hand is " + players1[i].gethandsum());
				
				if (players1[i].gethandsum()>21)
				{
					System.out.println(players1[i].getName() + ", sorry! You busted!"); 
					System.out.println(); {continue;}
				}
				System.out.println();
				{continue;}
			}
           
            if (choice=='h')
            {
            	
            	players1[i].addCard(deck.deal());
            	System.out.println(players1[i].gethandsum());
            	if (players1[i].gethandsum()>21)
            	{
            		System.out.println("Sorry " + players1[i].getName() + ", you busted!"); 
            		System.out.println();
            		{continue;}
            	}
            	System.out.println("Do you still want to keep on hitting? Hit y for yes, n for no.");
            	char choice2=IO.readChar();
            	while (choice2!='y'&&choice2!='n')
            	{
            		System.out.println("Enter your letter again.");
            		choice2=IO.readChar();
            	}
            	while (IO.readChar()=='y')
            	{
            		
            		players1[i].addCard(deck.deal());
            		System.out.println(players1[i].gethandsum());
            		
            		if (players1[i].gethandsum()>21)
            		{
            			System.out.println("Sorry " + players1[i].getName() +"! You busted!"); 
            			System.out.println(); 
            			{continue;}
            		}
            		
            		if (deck.isEmpty()==true)
            		{
            			System.out.println("We ran out of cards!");
            			return;
            		}
            		

            	}
            	if (choice2=='n') 
            	{
            		System.out.println();
            		{continue;}
            	}
            }
            if (choice=='s') 
            {
            	System.out.println();
            	{continue;}
            }
			}
			
			System.out.println();
		}
		
		System.out.println();
		System.out.println("Now it's the dealer's turn to play.");
		//dealer.addCard(deck.deal());
		if (deck.isEmpty()==true)
		{
			System.out.println("We ran out of cards!");
			return;
		}
		//Card k=deck.deal();
        dealer.addCard(b);
       // int p=dealer.cardValue(k);
       // dealer.isBlackJack();
		System.out.println("After flipping the card over, the dealer's score is now " + dealer.gethandsum());
		if (a.getValue()==11&&dealer.gethandsum()!=21)
		{
			System.out.println("The dealer's face down card did not have a value of 10.");
			for (int i=0;i<players1.length;i++)
			{
			System.out.println(players1[i].getName() + ", your balance is now " + players1[i].loseInsurance());
			}
		}
		
		
		if (a.getValue()==11&&dealer.gethandsum()==21)
		{
			
			System.out.println("The face down card was a 10! The dealer has blackjack!");
			System.out.println("Players who decided to have insurance bets will be rewarded.");
			for (int i=0;i<players1.length;i++)
			{
				System.out.println(players1[i].getName() + ", your balance is now " + players1[i].winInsurance());
			}
		}
		
		while (dealer.gethandsum()<17)
		{
			if (deck.isEmpty()==true)
			{
				System.out.println("We ran out of cards!");
				return;
			}
			dealer.addCard(deck.deal());
			System.out.println("Dealer's current score is " + dealer.gethandsum());
			
			
				
			if (dealer.gethandsum()>21)
			{
				System.out.println("The dealer busted!"); {break;}
			}
		}
		
		System.out.println();
		System.out.println("Dealer's final score is " + dealer.gethandsum());
		
		
		System.out.println();
		System.out.println("Now to determine who won and who lost.");
		System.out.println();
		for (int i=0;i<players1.length;i++)
		{
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()<=21&&dealer.gethandsum()>21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", you won!");
				System.out.println("Your balance is now " + players1[i].splitWin());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum()>dealer.gethandsum()&&players1[i].gethandsum2()>dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", you won!");
				System.out.println("Your balance is now " + players1[i].splitWin());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum()>dealer.gethandsum()&&players1[i].gethandsum2()<dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", you tied with the dealer.");
				System.out.println("Your first hand beat the dealer's hand, but the second hand didn't.");
				System.out.println("Your balance is " + players1[i].getBalance());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum()<dealer.gethandsum()&&players1[i].gethandsum2()>dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", you tied with the dealer.");
				System.out.println("Your second hand beat the dealer's hand, but the first hand didn't.");
				System.out.println("Your balance is " + players1[i].getBalance());
				System.out.println();
			}
			
			if (players1[i].gethandsum()==dealer.gethandsum()&&players1[i].gethandsum2()==dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", you tied with the dealer.");
				System.out.println("Your balance is now " + players1[i].getBalance());
				System.out.println();
			}
			
			if (players1[i].gethandsum()>21&&players1[i].gethandsum2()>21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", both of your hands busted!");
				System.out.println("Your balance is now " + players1[i].splitLose());
				System.out.println();
			}
			
			if (players1[i].gethandsum()>21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum2()<dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", your first hand busted and your second hand lost against the dealer's hand.");
				System.out.println("Your balance is now " + players1[i].splitLose());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()>21&&players1[i].gethandsum()<dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", your second hand busted and your first hand lost against the dealer's hand.");
				System.out.println("Your balance is now " + players1[i].splitLose());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()>21&&players1[i].gethandsum()==dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", your second hand busted and your first hand tied against the dealer.");
				System.out.println("Your balance is now " + players1[i].splitTie());
				System.out.println();
			}
			
			if (players1[i].gethandsum()>21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum2()==dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{	
				System.out.println(players1[i].getName() + ", your first hand busted and your second hand tied against the dealer's hand.");
				System.out.println("Your balance is now " + players1[i].splitTie());
				System.out.println();
			}
			
			if (players1[i].gethandsum()>21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum2()>dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", your first hand busted but your second hand beat the dealer's hand.");
				System.out.println("Your balance is now " + players1[i].getBalance());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()>21&&players1[i].gethandsum()>dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", your second hand busted but your first hand beat the dealer's hand.");
				System.out.println("Your balance is now " + players1[i].getBalance());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum()<dealer.gethandsum()&&players1[i].gethandsum2()<dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", both of your hands lost against the dealer's hand.");
				System.out.println("Your balance is now " + players1[i].splitLose());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum()<dealer.gethandsum()&&players1[i].gethandsum2()==dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", your first hand lost against the dealer's hand and your second hand pushed.");
				System.out.println("Your balance is now " + players1[i].lose());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum()==dealer.gethandsum()&&players1[i].gethandsum2()<dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", your first hand pushed but your second hand lost against the dealer's hand.");
				System.out.println("Your balance is now " + players1[i].lose());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum()==dealer.gethandsum()&&players1[i].gethandsum2()>dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", your second hand beat the dealer's hand but the first hand tied.");
				System.out.println("Your balance is now " + players1[i].splitTie2());
				System.out.println();
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum2()<=21&&players1[i].gethandsum()>dealer.gethandsum()&&players1[i].gethandsum2()==dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2!=null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", your first hand beat the dealer's hand but the second hand tied.");
				System.out.println("Your balance is now " + players1[i].splitTie2());
				System.out.println();
			}
            
			
			//END OF SPLITTING WIN/LOSE/TIE SCENARIOS
			

			if (players1[i].blackjack==true&&players1[i].gethandsum()==21&&players1[i].hand2==null&&dealer.gethandsum()<21&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", you won Blackjack!");
				System.out.println("Your balance is now " + players1[i].winBlackJack());
				System.out.println();
			}
			
			if (players1[i].blackjack==true&&players1[i].gethandsum()>21&&players1[i].hand2==null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", you busted!");
				System.out.println("Your balance is now " + players1[i].lose());
				System.out.println();
			}
			
			if (players1[i].blackjack==true&&players1[i].gethandsum()==21&&dealer.gethandsum()==21&&players1[i].hand2==null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", you busted with the dealer.");
				System.out.println("Your balance is now " + players1[i].getBalance());
				System.out.println();
			}
			
			if (players1[i].blackjack==true&&players1[i].gethandsum()==21&&dealer.gethandsum()>21&&players1[i].hand2==null&&players1[i].getBalance()>0)
			{
				System.out.println(players1[i].getName() + ", you won!");
				System.out.println("Your balance is now " + players1[i].winBlackJack());
				System.out.println();
			}
			
			
			// END OF BLACKJACK WINNING SCENARIO
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum()>dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].getBalance()>0&&players1[i].hand2==null&&players1[i].blackjack==false)
			{
				System.out.println(players1[i].getName() + ", you won!");
				System.out.println("Your balance is now " + players1[i].win());
				System.out.println();
				
			}
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum()==dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].getBalance()>0&&players1[i].hand2==null&&players1[i].blackjack==false)
			{
				System.out.println(players1[i].getName() + ", you tied with the dealer.");
				System.out.println("Your balance is now " + players1[i].getBalance());
				System.out.println();
			}
			
			
			if (players1[i].gethandsum()<=21&&players1[i].gethandsum()<dealer.gethandsum()&&dealer.gethandsum()<=21&&players1[i].hand2==null&&players1[i].blackjack==false&&players1[i].getBalance()>0)
			{
				System.out.println("Sorry " + players1[i].getName() + ", you did not win.");
				System.out.println("Your balance is now " + players1[i].lose());
				System.out.println();
			}
			
			if (players1[i].gethandsum()>21&&players1[i].hand2==null&&players1[i].blackjack==false&&players1[i].getBalance()>0)
			{
				System.out.println("Sorry " + players1[i].getName() + ", you did not win.");
				System.out.println("Your balance is now " + players1[i].lose());
				System.out.println();
			}
			
			if (dealer.gethandsum()>21&&players1[i].gethandsum()<=21&&players1[i].getBalance()>0&&players1[i].hand2==null&&players1[i].blackjack==false)
			{
				System.out.println(players1[i].getName() + ", you won!");
				System.out.println("Your balance is now " + players1[i].win());
				System.out.println();
				
			}
			
			
			if (players1[i].getBalance()<=0&&players1[i].hand2==null)
			{
				System.out.println(players1[i].getName() + ", you shouldn't even be playing anyway.");
				System.out.println();
			}
			

			if (players1[i].getBalance()<=0&&players1[i].hand2!=null)
			{
				System.out.println(players1[i].getName() + ", you shouldn't even be playing anyway.");
				System.out.println();
			}

		}
		
		System.out.println("Do you guys want to start another blackjack round?");
		System.out.println("Decide on an answer as a group, and hit y for yes, n for no.");
		
		}
		while (IO.readChar()=='y');
		}
	}
